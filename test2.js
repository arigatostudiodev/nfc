var device;

function startDevice(){
  navigator.usb.requestDevice({ filters: [{ vendorId: 0x072f }] })
    .then(device => {
      console.log(device.productName);      // "Arduino Micro"
      console.log(device.manufacturerName); // "Arduino LLC"
    })
    .catch(error => { console.log(error); });
}

function getDevice(){
  navigator.usb.getDevices().then(devices => {
    devices.map(device => {
      console.log(device.productName);      // "Arduino Micro"
      console.log(device.manufacturerName); // "Arduino LLC"
    });
  })
}

function claimDevice(){
  navigator.usb.requestDevice({ filters: [{ vendorId: 0x072f }] })
    .then(selectedDevice => {
       device = selectedDevice;
       return device.open(); // Begin a session.
     })
    .then(() => device.selectConfiguration(1)) // Select configuration #1 for the device.
    .then(() => device.claimInterface(device.configuration.interfaces[0].interfaceNumber)) // Request exclusive control over interface #2.
    .then(() => device.transferOut(deviceEndpoint, powerUpDevice)
      .then(transferResult => {
        console.log(transferResult);
      }, error => {
        console.log(error);
        device.close();
      })
      .catch(error => {
        console.log(error);
      }))
    .then(() => device.transferIn(2, 64)) // Waiting for 64 bytes of data from endpoint #5.
    .then(result => {
      let decoder = new TextDecoder();
      console.log('Received: ' + decoder.decode(result.data));
    })
    .catch(error => { console.log(error); });
}
